# NLC Assembly Test Sandbox

Just and app to test nasm and clang assembly with Qt

=== Flatpak build NLC Assembly Test Sandbox ===
git clone https://gitlab.com/nolimitconnectapps/nlc-assembly-test-sandbox.git
cd ~/nlc-assembly-test-sandbox
flatpak-builder build-dir com.nolimitconnect.Assembly1234TestApp.yaml --force-clean
flatpak-builder --user --install --force-clean build-dir com.nolimitconnect.Assembly1234TestApp.yml
flatpak run com.nolimitconnect.Assembly1234TestApp

