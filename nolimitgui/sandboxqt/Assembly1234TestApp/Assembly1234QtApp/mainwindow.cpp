#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <AssemblyLib.h>

#if !defined(TARGET_OS_WINDOWS)
# include <unistd.h>
#endif // !defined(TARGET_OS_WINDOWS)

namespace{
bool  getExecutePathAndName( std::string& strRetExeDir, std::string& strRetExeFileName )
{
#ifdef TARGET_OS_WINDOWS
    wchar_t pRetBuf[ VX_MAX_PATH ];
    int iRetStrLen = GetModuleFileNameW( nullptr, pRetBuf, VX_MAX_PATH );
    if( 0 != iRetStrLen )
    {
        // remove file name
        wchar_t * pTemp = wstrrchr( pRetBuf, '\\' );
        if( pTemp )
        {
            * pTemp = 0;
            pTemp++;
            strRetExeFileName = WideToUtf8( pTemp );
        }
        // remove debug path if exists
        pTemp = wstrrchr( pRetBuf, '\\' );
        if( pTemp )
        {
#ifdef _DEBUG
            if( 0 == wstrcmp( pTemp, L"\\DEBUG" ) ||
                0 == wstrcmp( pTemp, L"\\Debug" ) )
            {
                *pTemp = 0;
            }
#endif
        }
        // make sure has the final slash
        if( pRetBuf[ wstrlen( pRetBuf ) - 1 ] != '\\' )
        {
            wstrcat( pRetBuf, L"\\" );
        }
        // flip the slashes
        size_t uiStrLen = wstrlen( pRetBuf );
        for( size_t i = 0; i < uiStrLen; i++ )
        {
            if( L'\\' == pRetBuf[ i ] )
            {
                pRetBuf[ i ] = L'/';
            }
        }

        strRetExeDir = WideToUtf8( pRetBuf );
        return truee;
    }
    else
    {
        //LogMsg( LOG_INFO, "Error %d occurred getting module directory", VxGetLastError());
        return false;
    }

#else // LINUX
    char pRetBuf[ 1024 ];
    int iByteCount;
    char* pTempBuf;
    iByteCount = readlink("/proc/self/exe", pRetBuf, 1024);
    if(-1 == iByteCount)
    {
        //LogMsg( LOG_INFO, "Error %d occured getting module directory", VxGetLastError());
        return false;

    }
    pRetBuf[iByteCount] = '\0';

    if(nullptr == (pTempBuf = strrchr(pRetBuf,'/')))
    {
        //LogMsg( LOG_INFO, "Error %d occured getting module directory", VxGetLastError());
        return false;
    }
    pTempBuf[1] = '\0';

    strRetExeFileName = &pTempBuf[2];
    strRetExeDir = pRetBuf;
#endif // LINUX
    return true;
}

};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->m_CButton, SIGNAL(clicked()), this, SLOT(slotCButtonClicked()));
    connect(ui->m_CppButton, SIGNAL(clicked()), this, SLOT(slotCppButtonClicked()));
    connect(ui->m_AssemblyButton, SIGNAL(clicked()), this, SLOT(slotAssemblyButtonClicked()));
    connect(ui->m_AssemblyReverseButton, SIGNAL(clicked()), this, SLOT(slotAssemblyReverseButtonClicked()));
    connect(ui->m_TestFilesButton, SIGNAL(clicked()), this, SLOT(slotTestFilesButtonClicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::LogMsg( uint32_t u32MsgType, const char* msg, ...)
{
#define MAX_ERR_MSG_SIZE 4096
    std::string strData;
    strData.reserve( MAX_ERR_MSG_SIZE );

    va_list argList;
    va_start( argList, msg );
    vsnprintf( (char*)strData.c_str(), MAX_ERR_MSG_SIZE, msg, argList );
    ((char*)strData.c_str())[MAX_ERR_MSG_SIZE - 2] = 0;
    ui->m_OutputTextLabel->setText(strData.c_str());
}

void MainWindow::slotCButtonClicked()
{
    ui->m_OutputTextLabel->setText(getCText());
}

void MainWindow::slotCppButtonClicked()
{
    ui->m_OutputTextLabel->setText(getCppText());
}

void MainWindow::slotAssemblyButtonClicked()
{
    ui->m_OutputTextLabel->setText(QString::number(get_assembly_test_num()));
}

void MainWindow::slotAssemblyReverseButtonClicked()
{
#if defined(TARGET_OS_ANDROID)
    // android has 2 assembly files (.s and .S) and one of them contains get_assembly_reverse_num
    ui->m_OutputTextLabel->setText(QString::number(get_assembly_reverse_num()));
#else
    // linux/windows has 1 .asm assembly file so fake the missing get_assembly_reverse_num
    ui->m_OutputTextLabel->setText("4321");
#endif // defined(TARGET_OS_ANDROID)
}

void MainWindow::slotTestFilesButtonClicked()
{
    std::string strExeDir;;
    std::string strExeFileName;
    if(!getExecutePathAndName( strExeDir, strExeFileName ))
    {
        ui->m_OutputTextLabel->setText("Failed  get exe path");
        return;
    }

    //std::string testFile1 = strExeDir +  "assets/TestFile1.txt";
    std::string testFile1 = "assets/TestFile1.txt";
    if(!firstCharInFileIs(testFile1, '1'))
    {
        LogMsg(LOG_DEBUG, "Failed read TestFile1 %s",  testFile1.c_str());
        return;
    }

    //std::string testFile2 = strExeDir +  "assets/assetsubdir/TestFile2.txt";
    std::string testFile2 = "assets/assetsubdir/TestFile2.txt";
    if(!firstCharInFileIs(testFile2, '2'))
    {
        LogMsg(LOG_DEBUG, "Failed read TestFile2 %s",  testFile2.c_str());
        return;
    }

    ui->m_OutputTextLabel->setText("Read Files Test OK");
}

bool MainWindow::firstCharInFileIs(std::string fileName, char expectedChar)
{
    FILE* poFile = fopen( fileName.c_str(), "rb" );
    if( nullptr == poFile )
    {
        ui->m_OutputTextLabel->setText("firstCharInFileIs: error opening file");
        return false;
    }
    
    char firstChar = ' ';

    size_t iResult = fread( &firstChar, 1, 1, poFile );
    fclose( poFile );
    return iResult > 0 && expectedChar == firstChar;
}
