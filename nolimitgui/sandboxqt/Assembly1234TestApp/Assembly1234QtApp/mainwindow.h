#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define LOG_NONE    (0x0000)
#define LOG_VERBOSE (0x0001)
#define LOG_FATAL	(0x0002)
#define LOG_SEVERE	(0x0004)
#define LOG_ASSERT	(0x0008)
#define LOG_ERROR	(0x0010)
#define LOG_WARN	(0x0020)
#define LOG_DEBUG	(0x0040)
#define LOG_INFO	(0x0080)
#define LOG_STATUS	(0x0100)
#define LOG_PRIORITY_MASK	    0x000001ff

// defines so less work converting Linux code
#define LOG_WARNING		LOG_WARN
#define LOG_ERR			LOG_ERROR
#define LOG_CRIT		LOG_FATAL

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void							LogMsg( uint32_t u32MsgType, const char* msg, ...);

protected slots:
    void slotCButtonClicked();
    void slotCppButtonClicked();
    void slotAssemblyButtonClicked();
    void slotAssemblyReverseButtonClicked();
    void slotTestFilesButtonClicked();

protected:
    bool firstCharInFileIs(std::string fileName, char expectedChar);

    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
