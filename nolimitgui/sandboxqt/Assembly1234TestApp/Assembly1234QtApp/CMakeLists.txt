cmake_minimum_required(VERSION 3.22.1)

#=== project ===#
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt6 REQUIRED COMPONENTS Core Gui Widgets)

qt_add_executable(Assembly1234TestApp
    MANUAL_FINALIZATION
    mainwindow.h
    mainwindow.cpp
    main.cpp
    mainwindow.ui
)

if(DEFINED TARGET_OS_ANDROID)
    set_target_properties(Assembly1234TestApp PROPERTIES
        ANDROID_TARGET_ARCH arm64-v8a
        QT_ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_LIST_DIR}/../android"
        QT_ANDROID_ABIS arm64-v8a

    )
else()
    set_target_properties(Assembly1234TestApp PROPERTIES
        WIN32_EXECUTABLE TRUE
        MACOSX_BUNDLE TRUE
    )
endif()

target_include_directories(Assembly1234TestApp PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/../Assembly1234TestLib
)

target_link_libraries(Assembly1234TestApp PUBLIC
    Assembly1234TestLib
    Qt::Core
    Qt::Gui
    Qt::Widgets
)

qt_finalize_target(Assembly1234TestApp)

if(NOT DEFINED FLATPAKBUILD)

    #=== after compile add qt libraries to bin-TargetOs directory ===#
    if(DEFINED TARGET_OS_LINUX)

        file(MAKE_DIRECTORY "${PROJECT_BIN_DIR}/usr/lib")

        #find_package(Qt6 REQUIRED COMPONENTS Core Gui Multimedia MultimediaWidgets OpenGL OpenGLWidgets Svg Widgets)
        add_custom_command(POST_BUILD TARGET Assembly1234TestApp
            WORKING_DIRECTORY "${PROJECT_BIN_DIR}"
            COMMAND ${CMAKE_COMMAND} -E copy_if_different "${CMAKE_PREFIX_PATH}/lib/libQt6Core.so*" "${PROJECT_BIN_DIR}/usr/lib"
            COMMAND ${CMAKE_COMMAND} -E copy_if_different "${CMAKE_PREFIX_PATH}/lib/libQt6Gui.so*" "${PROJECT_BIN_DIR}/usr/lib"
            COMMAND ${CMAKE_COMMAND} -E copy_if_different "${CMAKE_PREFIX_PATH}/lib/libQt6Widgets.so*" "${PROJECT_BIN_DIR}/usr/lib"
            COMMAND_EXPAND_LISTS
        )


        #=== after qt libraries are copied then make installer with cpack ===#

        # common to all packages
        set(CPACK_PACKAGE_NAME "${NLC_NAME}")
        set(CPACK_PACKAGE_VENDOR "${PROJECT_VENDOR}")
        set(CMAKE_PROJECT_HOMEPAGE_URL  "${PROJECT_WEBSITE_URL}")
        set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
            "Assembly1234TestApp - Just an app testing nasm and clang assembly with qt")
        set(CPACK_PACKAGE_VERSION "${NLC_VERSION}")
        set(CPACK_PACKAGE_VERSION_MAJOR "${NLC_VERSION_MAJOR}")
        set(CPACK_PACKAGE_VERSION_MINOR "${NLC_VERSION_MINOR}")
        set(CPACK_PACKAGE_VERSION_PATCH "${NLC_VERSION_PATCH}")

        set(CPACK_SET_DESTDIR TRUE)
        set(CPACK_RESOURCE_FILE_README "${PROJECT_SRC_ROOT_DIR}/README.md")
        set(CPACK_RESOURCE_FILE_LICENSE "${PROJECT_SRC_ROOT_DIR}/LICENSE")

        if(DEFINED TARGET_OS_LINUX)
            set(CPACK_GENERATOR "DEB")
            include(GNUInstallDirs)
            set(CPACK_DEBIAN_PACKAGE_MAINTAINER "${PROJECT_MAINTAINER}")
            set(CPACK_DEBIAN_PACKAGE_DEPENDS "libpthread")
            set(CPACK_PACKAGE_DIRECTORY "${PROJECT_SRC_ROOT_DIR}/package/linux")

        elseif(DEFINED TARGET_OS_WINDOWS)
            set(CPACK_GENERATOR "NSIS")
            set(CPACK_NSIS_MUI_ICON "${PROJECT_SRC_ROOT_DIR}/nolimitgui/CommonSrc/Resources/NlcP2P.ico")
            set(CPACK_PACKAGE_DIRECTORY "${PROJECT_SRC_ROOT_DIR}/package/windows")

        elseif(DEFINED TARGET_OS_ANDROID)
            set(CPACK_PACKAGE_DIRECTORY "${PROJECT_SRC_ROOT_DIR}/package/android")

        endif()

        # This must always be after all CPACK\_\* variables are defined
        include(CPack)
    endif()

    if(DEFINED TARGET_OS_ANDROID)

        message(STATUS "binary directory gui ${CMAKE_CURRENT_BINARY_DIR}")
    #F:\build-nlc-assembly-test-sandbox-Android_Qt_6_4_3_Clang_arm64_v8a-Debug\nolimitgui\sandboxqt\Assembly1234TestApp\Assembly1234QtApp\android-build\build\outputs\apk\debug

        # this wont work because tries to copy the apk before the apk has been compiled
        #add_custom_command(POST_BUILD TARGET Assembly1234TestApp
        #    WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
        #    COMMAND ${CMAKE_COMMAND} -E copy_if_different "${CMAKE_CURRENT_BINARY_DIR}/nolimitgui/sandboxqt/Assembly1234TestApp/Assembly1234QtApp/android-build/build/outputs/apk/debug/*.apk" "${PROJECT_PACKAGE_DIR}/"
        #    COMMAND_EXPAND_LISTS
        #)
    endif()
else()

    set(INSTALL_DIR "") # empty to not complicate run command

    install(TARGETS Assembly1234TestApp
        RUNTIME DESTINATION "${INSTALL_DIR}"
        BUNDLE DESTINATION "${INSTALL_DIR}"
        LIBRARY DESTINATION "${INSTALL_DIR}"
       )

    # Install executable
    install(
        TARGETS Assembly1234TestApp
        DESTINATION "${CMAKE_INSTALL_PREFIX}"
    )

    message(STATUS "assets dir ${PROJECT_SRC_ROOT_DIR}/assets")

    # Install resources
    install(
        DIRECTORY "${PROJECT_SRC_ROOT_DIR}/assets"
        DESTINATION "${CMAKE_INSTALL_PREFIX}"
    )

    # Install desktop file
    install(
        FILES ${PROJECT_SRC_ROOT_DIR}/com.nolimitconnect.Assembly1234TestApp.desktop
        DESTINATION share/applications
    )

    # And install all the icons
    install(
        FILES ${PROJECT_SRC_ROOT_DIR}/icons/16x16/com.nolimitconnect.Assembly1234TestApp.png
        DESTINATION share/icons/hicolor/16x16/apps/
    )
    install(
        FILES ${PROJECT_SRC_ROOT_DIR}/icons/32x32/com.nolimitconnect.Assembly1234TestApp.png
        DESTINATION share/icons/hicolor/32x32/apps/
    )
    install(
        FILES ${PROJECT_SRC_ROOT_DIR}/icons/48x48/com.nolimitconnect.Assembly1234TestApp.png
        DESTINATION share/icons/hicolor/48x48/apps/
    )
    install(
        FILES ${PROJECT_SRC_ROOT_DIR}/icons/64x64/com.nolimitconnect.Assembly1234TestApp.png
        DESTINATION share/icons/hicolor/64x64/apps/
    )
    install(
        FILES ${PROJECT_SRC_ROOT_DIR}/icons/128x128/com.nolimitconnect.Assembly1234TestApp.png
        DESTINATION share/icons/hicolor/128x128/apps/
    )
    install(
        FILES ${PROJECT_SRC_ROOT_DIR}/icons/256x256/com.nolimitconnect.Assembly1234TestApp.png
        DESTINATION share/icons/hicolor/256x256/apps/
    )
    install(
        FILES ${PROJECT_SRC_ROOT_DIR}/icons/512x512/com.nolimitconnect.Assembly1234TestApp.png
        DESTINATION share/icons/hicolor/512x512/apps/
    )
    # the max allowed icon size for flathub is 512x512
    #if(NOT FLATPAKBUILD)
    #    install(
    #        FILES ${PROJECT_SRC_ROOT_DIR}/icons/1024x1024/com.nolimitconnect.Assembly1234TestApp.png
    #        DESTINATION share/icons/hicolor/1024x1024/apps/
    #    )
    #endif()


    install(
        FILES ${PROJECT_SRC_ROOT_DIR}/com.nolimitconnect.Assembly1234TestApp.metainfo.xml
        DESTINATION share/metainfo/
    )

endif() # DEFINED FLATPAKBUILD
